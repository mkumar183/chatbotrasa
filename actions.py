from __future__ import absolute_import
from __future__ import division
from __future__ import unicode_literals

from rasa_sdk import Action
from rasa_sdk.events import SlotSet
import zomatopy
import json
import pandas as pd

f = open(".\locations.txt", "r")
valid_locations = [line.strip() for line in f.readlines()]


#importing the Yagmail library
import yagmail



class ActionSearchRestaurants(Action):
    def name(self):
        return 'action_search_restaurants'

    def run(self, dispatcher, tracker, domain):
        config = {"user_key": "d483a96254f6de7f7e28e9f6464b4118"}
        zomato = zomatopy.initialize_app(config)
        loc = tracker.get_slot('location')
        price = tracker.get_slot('price_range')
        cuisine = tracker.get_slot('cuisine')
        print('loc, price, cuisine: ', loc, price, cuisine)
        data = []
        if loc in valid_locations:
            print("city is valid")
            location_detail = zomato.get_location(loc, 1)
            d1 = json.loads(location_detail)
            lat = d1["location_suggestions"][0]["latitude"]
            lon = d1["location_suggestions"][0]["longitude"]
            cuisines_dict = {'bakery': 5, 'chinese': 25, 'cafe': 30, 'italian': 55, 'biryani': 7, 'north indian': 50,
                             'south indian': 85}
            response = ""

            for s in [0,20,40,60,80]:
                print('fetching data start = ', s)
                results = zomato.restaurant_search(query="", latitude=lat, longitude=lon, cuisines=str(cuisines_dict.get(cuisine)),limit=100, start=s)
                d = json.loads(results)
                print('results', d['results_found'])
                if d['results_found'] == 0:
                    response = "no results"
                    break
                else:
                    for restaurant in d['restaurants']:
                        response = response + "Found " + restaurant['restaurant']['name'] + " in " + \
                                   restaurant['restaurant']['location']['address'] + \
                                   " average cost for 2 people: " +  str(restaurant['restaurant']['average_cost_for_two']) + \
                                   " rating: " + str(restaurant['restaurant']['user_rating']['aggregate_rating']) + "\n"
                        data.append([restaurant['restaurant']['name'],restaurant['restaurant']['location']['address'],restaurant['restaurant']['average_cost_for_two'],restaurant['restaurant']['user_rating']['aggregate_rating'] ])
        else:
            response = "We do not operate in that area yet"
            print("invalid city")

        df = pd.DataFrame(data, columns= ['name','address','price','rating'])
        # need to use price
        # get top 5 restaurants
        if price == "Less than 300":
            df1 = df.loc[df.price < 300][:5]
        elif price == "between 300 and 700":
            df1 = df.loc[(df.price >= 300) & (df.price < 700)][:5]
        else:
            df1 = df.loc[df.price >= 700][:5]

        message = ""
        for ind in df1.index:
            message = message + "Found: " + df1['name'][ind] + "Address: " + df1['address'][ind] + "price: " + str(df1['price'][ind]) + "rating: " + str(df1['rating'][ind]) + "\n"
        print("final message: ", message)

        # for email we need 10 restaurants
        df1 = df.loc[df.price >= 700][:10]
        message2 = ""
        for ind in df1.index:
            message2 = message2 + "Found: " + df1['name'][ind] + "Address: " + df1['address'][ind] + "price: " + str(df1['price'][ind]) + "rating: " + str(df1['rating'][ind]) + "\n"

        dispatcher.utter_message("-----" + message)
        return [SlotSet('location', loc), SlotSet('message', message2)]

class ActionSendEmail(Action):
    def name(self):
        return 'action_send_email'

    def send_email(self, to_mail, message):
        try:
            yag = yagmail.SMTP(user='mkumariiitbassignments@gmail.com', password='L2j58wGI123')
            yag.send(to=to_mail, subject='List of Restaurants you Asked ! ', contents=message)
            print("Email sent successfully")
        except Exception as e:
            print("Error, email was not sent")
            print(str(e))

    def run(self, dispatcher, tracker, domain):
        message = tracker.get_slot('message')
        print('inside action send email' , message)
        to_mail = tracker.get_slot('emailid')
        loc = tracker.get_slot('location')
        print('to_mail' , to_mail)

        self.send_email(to_mail, message)
        dispatcher.utter_message("message sent")
        return [SlotSet('location', loc)]

