changes made in this project:
domain.yml
entities , slots and intents are added here .
- action_send_email , utter_ask_email & utter_ask_price_range actions are added
- emailid and price_range entities are adeed
- negate and email intents are added
- options for utter_ask_price_range are added
- price_range and emailid slots are added

stories.md
Stores all the stories. This is the file where i should have more stories. but due to time constraint unable to do so.
stories where first user asks for a restaurant then location cuisine and price

nlu.md
Added examples for each intent and entities. Also added regex for emailid entity and synonyms for few cities.
location.txt is used to read all T1 and T2 cities. For cities outside location.txt a message is displayed
"we do no operate in this city yet" in actions.py

actions.py
this is where 2 actions are coded.
action_search_restaurants
- added code for validating city from location.txt and tested it.
- retrieve all three slots (city, price and cuisine)
- made changes to call zomato api with start option 0 to 80.
- added logic to get data in a dataframe then sort by rating
- shortlist data based on price range.
- display 5 top choices to the user
- store 10 top choices in a new slot message in case user wants this to be sent in email

ActionSendEmail
- upon user providing emailid this action is called
- an email is sent by retrieving 10 restaurant list from messages slot

console.py
- this has to be saved in rasa core to increase the default timeout time
DEFAULT_STREAM_READING_TIMEOUT_IN_SECONDS = 30

